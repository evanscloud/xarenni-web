import Vue from "vue";
import Router from "vue-router";

import {
  auth
} from "./firebase.config";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: "/",
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    };
  },
  routes: [
    {
      path: '*',
      redirect: '/'
    }, {
      path: "/",
      component: (resolve) => {
        require(["./views/public/Start"], resolve)
      },
      children: [
        {
          path: "/",
          component: (resolve) => {
            require(["./views/public/Home"], resolve)
          }
        }, {
          path: "/excursions",
          component: (resolve) => {
            require(["./views/public/Excursions"], resolve)
          }
        },
        {
          path: "/excursions/:id",
          component: (resolve) => {
            require(["./views/public/Detail"], resolve)
          },
          props: true
        },
        {
          path: "/parties/:id",
          component: (resolve) => {
            require(["./views/public/Detail"], resolve)
          },
          props: true
        },
        {
          path: "/checkout/:id",
          component: (resolve) => {
            require(["./views/public/Checkout"], resolve)
          },
          props: true
        },
        {
          path: "/contact",
          component: (resolve) => {
            require(["./views/public/Contact"], resolve)
          }
        },

        {
          path: "/transfers",
          component: (resolve) => {
            require(["./views/public/Transfers"], resolve)
          }
        },

        {
          path: "/about",
          component: (resolve) => {
            require(["./views/public/About"], resolve)
          }
        },
        {
          path: "/privacy",
          component: (resolve) => {
            require(["./views/public/Privacy"], resolve)
          }
        },

        {
          path: '/account',
          component: (resolve) => {
            require(['./views/public/Account'], resolve)
          },
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "/login",
          component: (resolve) => {
            require(["./views/public/Login"], resolve)
          },
          meta: {
            redirectIfLogged: true
          }
        }
      ],
    },
    {
      path: "/admin",
      component: (resolve) => {
        require(["./views/admin/Dashboard"], resolve);
      },
      children: [
        {
          path: "/",
          component: (resolve) => {
            require(["./views/admin/Login"], resolve);
          },
          meta: {
            redirectIfAsAdminLogged: true
          },
        },
        {
          path: "dashboard/excursions",
          component: (resolve) => {
            require(["./views/admin/Excursions"], resolve);
          },
          meta: {
            requiresAdmin: true
          }
        },
        {
          path: "dashboard/excursions/:id",
          component: (resolve) => {
            require(["./views/admin/Excursion"], resolve);
          },
          meta: {
            requiresAdmin: true
          },
          props: true
        },
        {
          path: "dashboard/reservations",
          component: (resolve) => {
            require(["./views/admin/Reservations"], resolve);
          },
          meta: {
            requiresAdmin: true
          }
        },
      ],
    },
  ]
});

router.beforeEach((to, from, next) => {
  // TODO: Validate the login when that thing be ready
  let currentUser = auth.currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  let redirectIfLogged = to.matched.some(record => record.meta.redirectIfLogged)

  let isAdmin = auth.currentUser != null && (auth.currentUser.uid == "YrXl1GChnJMX1zKdVOOO7ixdeML2" || auth.currentUser.uid == "Fa9qXA5WigYCPOTRMyalHVtETGM2" || auth.currentUser.uid == "gOHe1UaRLUeHq8fDHetDdKJ6WO12");
  let requiresAdmin = to.matched.some(record => record.meta.requiresAdmin)
  let redirectIfAsAdminLogged = to.matched.some(record => record.meta.redirectIfAsAdminLogged)

  if (requiresAuth && !currentUser) return next({
    path: '/login'
  })

  if (redirectIfLogged && currentUser) return next({
    path: '/account'
  })

  if (requiresAdmin && !isAdmin) return next({
    path: '/admin'
  })

  if (redirectIfAsAdminLogged && isAdmin) return next({
    path: '/admin/dashboard/excursions'
  })

  return next()
})

export default router;