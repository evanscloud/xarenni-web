import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

// Helpers
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#0091EA',
    secondary: '#01579B',
    sun: '#ffbe00',
    blacktone:'#3d3100'
  }
})
