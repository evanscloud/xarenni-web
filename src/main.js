import Vue from "vue";
import './plugins/vuetify'
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;

/* Lodash injection */
import lodash from "lodash";
Object.defineProperty(Vue.prototype, "$lodash", {
  value: lodash
});

/* Moment injection */
import moment from "moment";
Object.defineProperty(Vue.prototype, "$moment", {
  value: moment
});

let app;

import "./firebase.config";
import {
  auth
} from "./firebase.config";
auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
  }
});

