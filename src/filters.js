import moment from "moment";

export function formatCurrency(amount = 0) {
    return amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

export function formatDate(timestamp, format = "DD/MM/YYYY hh:mm a") {
    if (timestamp === 0) {
        return "N/A"
    }
    return moment(timestamp).format(format);
}