import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyAUTLJXonqm0C7LYzvLbhYWuFg5An9f7pg",
    authDomain: "xarenni-web.firebaseapp.com",
    databaseURL: "https://xarenni-web.firebaseio.com",
    projectId: "xarenni-web",
    storageBucket: "xarenni-web.appspot.com",
    messagingSenderId: "149891887775",
    appId: "1:149891887775:web:d1870b5adda5b821"
};

firebase.initializeApp(config);

const auth = firebase.auth();

const db = firebase.firestore();

const reservations = db.collection("reservations");
const excursions = db.collection("excursions");
const parties = db.collection("parties");
const contact = db.collection("contact");
const cities = db.collection("cities");
const users = db.collection("users");

export {
    auth,
    db,
    users,
    cities,
    contact,
    parties,
    excursions,
    reservations
}